import socket

HOST = ''             # endress IP the servi
PORT = 5000            # Port that the servi is listen
BUFFER_SIZE = 20 #Normally 1024, but we fast response

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.bind((HOST, PORT))
tcp.listen(1)



while True:
    con, cliente = tcp.accept()
    print('Conectado por', cliente)
    while True:
        msg = con.recv(BUFFER_SIZE)
        if not msg: break
        print(cliente, msg)
        con.send(msg) #echo
    print('Finalizando conexao do cliente', cliente)
con.close()