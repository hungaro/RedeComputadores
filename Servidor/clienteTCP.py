import socket

HOST = '127.0.0.1'     # Endereco IP do Servidor
PORT = 5000            # Porta que o Servidor esta
BUFFER_SIZE = 1024

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.connect((HOST, PORT))

msg = input("Digite uma mensagem: ")
while msg != ' ':
    tcp.send(msg.encode('UTF-8'))
    data = tcp.recv(BUFFER_SIZE)
    print(data)
    msg = input("Digite uma mensagem: ")
tcp.close()